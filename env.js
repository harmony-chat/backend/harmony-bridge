module.exports = {
  // These are hardcoded partly from laziness and partly
  // from wanting to encourage users to run their own for security reasons
  discordToken: process.env.DISCORD_TOKEN,
  harmonyToken: process.env.HARMONY_TOKEN,
  ServiceTypes: {
    Discord: "discord",
    Harmony: "harmony"
  },
  HostBase: "http://localhost:3000/api"
};
