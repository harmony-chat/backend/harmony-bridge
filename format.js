module.exports = {
  message: m => ({
    id: m.id,
    content: m.content,
    authorId: m.author && m.author.id,
    channelId: m.channel.id,
    authorDisplayName:
      m.author && ((m.member && m.member.nick) || m.author.username)
  }),
  guild: g => ({
    id: g.id,
    name: g.name,
    icon: g.icon,
    ownerId: g.ownerID
  }),
  channel: c => ({
    id: c.id,
    name: c.name,
    guildId: c.guild && c.guild.id
  }),
  user: u => ({
    id: u.id,
    username: u.username
  }),
  invite: (g, i) => ({
    id: i.id,
    name: g.name,
    guildId: i.guildID,

    channelId: i.channelID
  })
};
