const express = require("express");
const app = express();
const apiRouter = require("./bridgeRest");
const bodyParser = require("body-parser");
const cors = require("cors");

const server = app.listen(3009, function() {
  app.use("/api", cors(), express.json(), apiRouter);
});

module.exports = {app, server};
