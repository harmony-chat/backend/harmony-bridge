const fetch = require("node-fetch");
const constants = require("./env");

async function request(method, path, noParse, body) {
  const token = constants.harmonyToken;
  console.log(body, "body", path);
  const response = await fetch(constants.HostBase + path, {
    method,
    headers: {
      Authorization: token,
      "Content-Type": body ? "application/json" : undefined
    },
    body: body && JSON.stringify(body),
    redirect: "follow"
  });
  if (response.status < 200 || response.status > 299) {
    const errorObject = new Error(response.statusText);
    errorObject.code = response.status;
    errorObject.body = await response.text();
    console.log(errorObject.body, path, method, "OOO");
    throw errorObject;
  }
  if (noParse) return await response.arrayBuffer();
  else return await response.json();
}

module.exports = {
  get: (path, noParse) => request("GET", path, noParse),
  head: (path, noParse) => request("HEAD", path, noParse),
  post: (path, body, noParse) => request("POST", path, noParse, body),
  put: (path, body, noParse) => request("PUT", path, noParse, body),
  delete: (path, body, noParse) => request("DELETE", path, noParse, body),
  patch: (path, body, noParse) => request("PATCH", path, noParse, body),

  request
};
