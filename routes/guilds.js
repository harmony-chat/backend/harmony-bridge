const router = require("express").Router();
const format = require("../format");
const client = require("../client");

router.get("/channels", function(req, res) {
  return res.json(req.guild.channels.map(channel => format.channel(channel)));
});

module.exports = router;
