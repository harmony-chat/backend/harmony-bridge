const router = require("express").Router();
const client = require("../client");
const format = require("../format");

router.get("/", function(req, res) {
  console.log("HAS CHANNEL?!", Object.keys(req.channel));
  return res.json(format.channel(req.channel));
});

router.post("/messages", async function(req, res) {
  const msg = await req.channel.createMessage({
    content: req.body.content
  });
  const formatted = format.message(msg);
  return res.json({
    ...formatted,
    nonce: req.body.nonce
  });
});

router.get("/messages", async function(req, res) {
  let limit = 50;
  if (req.query.limit) {
    const numLimit = Math.floor(req.query.limit);
    if (numLimit < 50 && numLimit >= 1) limit = numLimit;
  }

  const msgs = await req.channel.getMessages(
    limit,
    req.query.before,
    req.query.after
  );
  res.json(
    msgs
      .filter(msg => msg.type === 0 && msg.content)
      .map(msg => format.message(msg))
  );
});
module.exports = router;
