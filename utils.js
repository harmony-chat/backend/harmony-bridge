const client = require("./client");

function forwardSub(sessionId, msg) {
  const newGuildMap = {};
  const adapterSubscribe = {};
  for (const guildId in msg.d.g) {
    if (client.guilds.has(guildId)) {
      adapterSubscribe[guildId] = msg.d.g[guildId];
    } else {
      newGuildMap[guildId] = msg.d.g[guildId];
    }
  }
  return {newGuildMap, adapterSubscribe};
}

module.exports = {forwardSub};
