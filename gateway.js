const http = require("./http");
const WebSocket = require("@discordjs/uws");
const wss = new WebSocket.Server({server: http.server});
const format = require("./format");
const utils = require("./utils");
const msgpack = new (require("msgpack5"))();
const sessions = new Map();
const RestClient = require("./RestClient");
const client = require("./client");
const ops = require("./ops");
let gatewayUrl = null;

RestClient.get("/gateway").then(res => (gatewayUrl = res.url));

function sendFrame(sessionId, packet) {
  console.log("SENDING FRAME!", packet);
  sessions.get(sessionId).ws.send(msgpack.encode(packet));
}

function sendProxyFrame(sessionId, packet) {
  console.log("SENDING PROXY SERVER A FRAME!", packet);
  sessions.get(sessionId).proxyWS.send(msgpack.encode(packet));
}

async function onClientMessage(sessionId, frame) {
  const msg = msgpack.decode(frame);
  console.log("MESSAGE FROM CLIENT!", msg);
  switch (msg.o) {
    case ops.subscribe: {
      const {newGuildMap, adapterSubscribe} = utils.forwardSub(sessionId, msg);
      // Actually we ignore pub/sub.. for now
      if (Object.keys(newGuildMap).length)
        sendProxyFrame(sessionId, {
          o: msg.o,
          d: {
            g: newGuildMap,
            n: msg.d.n
          }
        });
      else
        sendFrame(sessionId, {
          o: msg.o,
          d: {
            n: msg.d.n
          }
        });
      break;
    }
    case ops.unsubscribe: {
      const {newGuildMap, adapterSubscribe} = utils.forwardSub(sessionId, msg);
      if (Object.keys(newGuildMap).length)
        sendProxyFrame(sessionId, {
          o: msg.o,
          d: {
            g: newGuildMap,
            n: msg.d.n
          }
        });
      else
        sendFrame(sessionId, {
          o: msg.o,
          d: {
            n: msg.d.n
          }
        });
      break;
    }
  }
  // sessions.get(sessionId).proxyWS.send(msg);
}

async function onProxyMessage(sessionId, msg) {
  console.log("PASSING ALONG PROXY MSG TO CLIENT!!", msgpack.decode(msg));
  sessions.get(sessionId).ws.send(msg);
}

const randomBytes = require("util").promisify(require("crypto").randomBytes);

wss.on("connection", async function(ws) {
  const proxyWS = new WebSocket(gatewayUrl);
  const sessionId = (await randomBytes(16)).toString("base64");
  ws.once("message", function(token) {
    sessions.set(sessionId, {proxyWS, ws});
    console.log("GOT TOKEN!", token);
    proxyWS.send(token);
    proxyWS.once("message", function() {
      sendFrame(sessionId, {
        o: ops.hello,
        d: {
          s: sessionId
        }
      });
      ws.on("message", onClientMessage.bind(null, sessionId));
      proxyWS.on("message", onProxyMessage.bind(null, sessionId));
    });
  });
});

function dispatch(event, data) {
  for (const sessionId of sessions.keys()) {
    sendFrame(sessionId, {
      o: ops.dispatch,
      d: {
        t: event,
        d: data
      }
    });
  }
}

client.on("messageCreate", function(msg) {
  dispatch("MESSAGE_CREATE", format.message(msg));
});

client.on("messageDelete", function(msg) {
  console.log("DELOOT!", Object.keys(msg));
  dispatch("MESSAGE_DELETE", format.message(msg));
});

client.on("messageUpdate", function(msg, old) {
  // For now, only listen for content updates as they're all we support
  if (old && old.content != msg.content) {
    console.log(msg.author.id, msg.channel.id);
    dispatch("MESSAGE_UPDATE", format.message(msg));
  }
});
