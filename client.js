const Client = require("eris");
const env = require("./env");
const client = new Client(env.discordToken);

client.on("ready", function() {
  console.log("Client connected!");
});
client.connect();

module.exports = client;
