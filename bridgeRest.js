const channelRouter = require("./routes/channels");
const snekfetch = require("snekfetch");
const format = require("./format");
const guildRouter = require("./routes/guilds");
// const userRouter = require("./routes/users");
const client = require("./client");
const RestClient = require("./RestClient");
const router = require("express").Router();
const Constants = require("./env");

router.get("/gateway", function(req, res) {
  res.json({
    url: `ws://${req.headers.host}`
  });
});

router.use("/channels/:channelId", function(req, res, next) {
  if (
    client.guilds.has(req.params.channelId) ||
    client.channelGuildMap[req.params.channelId]
  ) {
    req.service = Constants.ServiceTypes.Discord;
  } else {
    req.service = Constants.ServiceTypes.Harmony;
  }
  req.channelId = req.params.channelId;
  next();
});

router.get("/guilds", async function(req, res) {
  const guilds = await RestClient.get("/guilds");
  res.json(guilds.concat(client.guilds.map(guild => format.guild(guild))));
});

router.use("/guilds/:guildId", function(req, res, next) {
  if (client.guilds.has(req.params.guildId)) {
    req.service = Constants.ServiceTypes.Discord;
  } else {
    req.service = Constants.ServiceTypes.Harmony;
  }
  req.guildId = req.params.guildId;
  next();
});

router.use("/users/:userId", function(req, res, next) {
  if (client.users.get(req.params.userId)) {
    req.service = Constants.ServiceTypes.Discord;
  } else {
    req.service = Constants.ServiceTypes.Harmony;
  }
  req.userId = req.params.userId;
  next();
});

router.use(function(req, res, next) {
  // Amazingly, we don't even need to account for this being `undefined`!
  if (req.service == Constants.ServiceTypes.Harmony) {
    const request = snekfetch[req.method.toLowerCase()](
      Constants.HostBase + req.path
    );
    if (req.body) request.send(req.body);
    for (const header in req.headers) {
      request.set(header, req.headers[header]);
    }
    request.pipe(res);
    // return res.redirect(301, Constants.HostBase + req.path);
  } else {
    if (req.userId) req.user = client.users.get(req.userId);
    if (req.channelId) {
      const channelCollection = client.guilds.get(
        client.channelGuildMap[req.channelId] || req.channelId
      ).channels;
      req.channel = channelCollection.get(req.channelId);
    }
    if (req.guildId) req.guild = client.guilds.get(req.guildId);
    next();
  }
});

router.use("/guilds/:guildId", guildRouter);
router.use("/channels/:channelId", channelRouter);
// router.use("/users/:userId", userRouter);

module.exports = router;
